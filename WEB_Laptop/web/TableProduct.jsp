<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>DarkPan - Bootstrap 5 Admin Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@500;700&display=swap" rel="stylesheet"> 

        <!-- Icon Font Stylesheet -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="admin/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="admin/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" />

        <!-- Customized Bootstrap Stylesheet -->
        <link href="admin/css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="admin/css/style.css" rel="stylesheet">
        <style>
            .left-side {
                float: left;
                width: 50%;
            }

            .right-side {
                float: right;
                width: 50%;
            }
        </style>

    <div class="left-side">
        <!-- Các phần tử trong left-side -->
    </div>

    <div class="right-side">
        <!-- Các phần tử trong right-side -->
    </div>

</head>

<body>
    <div class="container-fluid position-relative d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->

        <%@include file="component/SideBarAdmin.jsp" %>

        <!-- Content Start -->
        <div class="content">
            <%@include file="component/navbarAdmin.jsp" %>
            <!-- Blank Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="container-fluid pt-4 px-4">
                    <div class="row g-4">
                        <div class="">
                            <div class="bg-secondary rounded h-100 p-4">
                                <h6 class="mb-4">Add Product</h6>
                                <form action="tableProduct" method="POST">
                                    <div class="left-side">
                                        <div class="col-md-6">
                                            <label class="form-label">Product Name</label>
                                            <input type="text" class="form-control col-4" name="name" >
                                        </div>
                                        <div class="col-md-6">  
                                            <label class="form-label">Category</label>
                                            <select class="form-select form-select-sm col-md-3" aria-label=".form-select-sm example" name="cate">
                                                <c:forEach var="c" items="${cate}">
                                                    <option  class="form-label" value="${c.getCategoryID()}">${c.getName()}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">Quantity </label>
                                            <input type="number" class="form-control col-3" name="quantity" >
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">Price </label>
                                            <input type="number" class="form-control col-3" name="price" >
                                        </div>
                                    </div>
                                    <div class="right-side">

                                        <div class="col-md-6">  
                                            <label class="form-label">Ram</label>
                                            <select class="form-select form-select-sm col-md-3" aria-label=".form-select-sm example" name="ram">
                                                <option  class="form-label" value="4GB">4GB</option>
                                                <option  class="form-label" value="8GB">8GB</option>
                                                <option  class="form-label" value="16GB">16GB</option>
                                                <option  class="form-label" value="32GB">32GB</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">  
                                            <label class="form-label">Storage</label>
                                            <select class="form-select form-select-sm col-md-3" aria-label=".form-select-sm example" name="storage">
                                                <option  class="form-label" value="256GB">256GB</option>
                                                <option  class="form-label" value="512GB">512GB</option>
                                                <option  class="form-label" value="1TB">1TB</option>
                                                <option  class="form-label" value="2TB">2TB</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">  
                                            <label class="form-label">CPU Brand</label>
                                            <select class="form-select form-select-sm col-md-3" aria-label=".form-select-sm example" name="cpu">
                                                <option  class="form-label" value="AMD">AMD</option>
                                                <option  class="form-label" value="Intel">Intel</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">  
                                            <label class="form-label">VGA Brand</label>
                                            <input type="text" class="form-control col-3" name="vga" >
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="module">
                                        <div class="mb-3">
                                            <label for="formFile" class="form-label">Image</label>
                                            <input class="form-control bg-dark" type="file" name="Image 0"id="formFile">
                                            <i class="fa fa-plus" id="addInput"></i>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Add new product</button>
                                </form>
                            </div>
                        </div> 
                        <div class="bg-secondary text-center rounded p-4">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h6 class="mb-0">All Product</h6>
                            </div>
                            <div class="table-responsive">
                                <table id="categoryTable" class="table text-start align-middle table-bordered table-hover mb-0">
                                    <thead>
                                        <tr class="text-white">
                                            <th scope="col">Product name</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Quantity Available</th>
                                            <th scope="col">Price</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="lp" items="${listProduct}">
                                            <tr>
                                                <td>${lp.getPname()}</td>
                                                <td>${lp.getName()}</td>
                                                <td>${lp.getQuantity()}</td>
                                                <td>${lp.getPrice()}</td>
                                                <c:if test="${la.getStatus() == 0}">

                                                </c:if>
                                                <c:if test="${la.getStatus() == 1}">

                                                </c:if>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div id="pagination"></div>
                        </div>
                    </div>
                </div>
                <!-- Blank End -->
                <!--Modal-->
                <!-- Footer Start -->
                <div class="container-fluid pt-4 px-4">
                    <div class="bg-secondary rounded-top p-4">
                        <div class="row">
                            <div class="col-12 col-sm-6 text-center text-sm-start">
                                &copy; <a href="#">Your Site Name</a>, All Right Reserved. 
                            </div>
                            <div class="col-12 col-sm-6 text-center text-sm-end">
                                <!--/*** This template is free as long as you keep the footer author?s credit link/attribution link/backlink. If you'd like to use the template without the footer author?s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                                Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                                <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Footer End -->
            </div>
            <!-- Content End -->


            <!-- Back to Top -->
            <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="admin/lib/chart/chart.min.js"></script>
        <script src="admin/lib/easing/easing.min.js"></script>
        <script src="admin/lib/waypoints/waypoints.min.js"></script>
        <script src="admin/lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="admin/lib/tempusdominus/js/moment.min.js"></script>
        <script src="admin/lib/tempusdominus/js/moment-timezone.min.js"></script>
        <script src="admin/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

        <!-- Template Javascript -->
        <script src="admin/js/main.js"></script>
        <script>
            var addButton = document.getElementById('addInput');
            var container = addButton.parentNode;
            var inputCount = 1;

            addButton.addEventListener('click', function () {
                var newInput = document.createElement('input');
                newInput.className = 'form-control bg-dark';
                newInput.type = 'file';
                newInput.name = 'Image' + inputCount;
                inputCount++;

                container.appendChild(newInput);
            });
        </script>
</body>

</html>