/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.ProductDTO;
import ProcessList.SortList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author msi
 */
public class ProductDAO extends DBcontext {

    public List<ProductDTO> getAllProduct() {
        String sql = "SELECT * FROM Product p JOIN Category c on c.CategoryID = p.CategoryID";
        List<ProductDTO> lp = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductDTO product = new ProductDTO();
                product.setProductID(rs.getInt(1));
                product.setCategoryID(rs.getInt(2));
                product.setPname(rs.getString(3));
                product.setQuantity(rs.getInt(4));
                product.setPrice(rs.getInt(5));
                product.setRam(rs.getString(6));
                product.setStorage(rs.getString(7));
                product.setCpu(rs.getString(8));
                product.setVga(rs.getString(9));
                product.setName(rs.getString(11));
                lp.add(product);
            }
        } catch (Exception e) {
        }
        return lp;
    }

    public ProductDTO getProductByID(int id) {
        String sql = "SELECT * FROM Product WHERE ProductID = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                ProductDTO product = new ProductDTO();
                product.setProductID(rs.getInt(1));
                product.setCategoryID(rs.getInt(2));
                product.setPname(rs.getString(3));
                product.setQuantity(rs.getInt(4));
                product.setPrice(rs.getInt(5));
                product.setRam(rs.getString(6));
                product.setStorage(rs.getString(7));
                product.setCpu(rs.getString(8));
                product.setVga(rs.getString(9));
                return product;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void addProduct(int CateID, String name, int quantity, double price, String ram, String storage, String cpu, String vga, int Status) {
        String sql = "INSERT INTO [dbo].[Product]\n"
                + "([CategoryID]\n"
                + ",[Name]\n"
                + "           ,[QuantityAvailable]\n"
                + "           ,[UnitPrice]\n"
                + "           ,[RamCapacity]\n"
                + "           ,[StorageCapacity]\n"
                + "           ,[CpuBrand]\n"
                + "           ,[VgaBrand]\n"
                + "           ,[Status])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, CateID);
            ps.setString(2, name);
            ps.setInt(3, quantity);
            ps.setDouble(4, price);
            ps.setString(5, ram);
            ps.setString(6, storage);
            ps.setString(7, cpu);
            ps.setString(8, vga);
            ps.setInt(9, Status);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<ProductDTO> getProductByCategory(int id) {
        String sql = "SELECT p.ProductID, p.Name, p.QuantityAvailable, p.UnitPrice, p.UnitPrice, p.RamCapacity, p.StorageCapacity, p.VgaBrand FROM Product p\n"
                + "JOIN Category c\n"
                + "ON c.CategoryID = p.CategoryID \n"
                + "WHERE c.CategoryID = ?";
        List<ProductDTO> lp = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductDTO pro = new ProductDTO();
                pro.setProductID(rs.getInt(1));
                pro.setPname(rs.getString(2));
                pro.setQuantity(rs.getInt(3));
                pro.setPrice(rs.getInt(4));
                pro.setRam(rs.getString(5));
                pro.setStorage(rs.getString(6));
                pro.setCpu(rs.getString(7));
                pro.setVga(rs.getString(8));
                lp.add(pro);
            }
        } catch (Exception e) {
        }
        return lp;
    }

    public List<ProductDTO> searchByKey(String key) {
        List<ProductDTO> list = new ArrayList<>();
        String sql = "SELECT * FROM Product WHERE Name LIKE '%" + key + "%'";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductDTO product = new ProductDTO();
                product.setProductID(rs.getInt(1));
                product.setCategoryID(rs.getInt(2));
                product.setPname(rs.getString(3));
                product.setQuantity(rs.getInt(4));
                product.setPrice(rs.getInt(5));
                product.setRam(rs.getString(6));
                product.setStorage(rs.getString(7));
                product.setCpu(rs.getString(8));
                product.setVga(rs.getString(9));
//                product.setStatus(rs.getInt(10));
                list.add(product);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<ProductDTO> filterProducts(String name, String brandCPU, String storage, String ram, String vga) {
        StringBuilder sql = new StringBuilder("SELECT * FROM dbo.Product p WHERE 1 = 1");
        List<ProductDTO> lp = new ArrayList<>();

        try {
            if (name != null && !name.isEmpty()) {
                sql.append(" AND p.Name LIKE '%" + name + "%'");
            }

            if (brandCPU != null && !brandCPU.isEmpty()) {
                sql.append(" AND p.CpuBrand LIKE '%" + brandCPU + "%'");
            }

            if (storage != null && !storage.isEmpty()) {
                sql.append(" AND p.StorageCapacity LIKE '%" + storage + "%'");
            }

            if (ram != null && !ram.isEmpty()) {
                sql.append(" AND p.RamCapacity LIKE '%" + ram + "%'");
            }
            if (vga != null && !vga.isEmpty()) {
                sql.append(" AND p.VgaBrand LIKE '%" + vga + "%'");
            }

            PreparedStatement ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ProductDTO product = new ProductDTO();
                product.setProductID(rs.getInt(1));
                product.setCategoryID(rs.getInt(2));
                product.setPname(rs.getString(3));
                product.setQuantity(rs.getInt(4));
                product.setPrice(rs.getDouble(5));
                product.setRam(rs.getString(6));
                product.setStorage(rs.getString(7));
                product.setCpu(rs.getString(8));
                product.setVga(rs.getString(9));
                lp.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lp;
    }

    public int getPidJustAdd() {
        String sql = "SELECT TOP 1 ProductID FROM Product ORDER BY ProductID DESC";
        int id;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public void addImageOfProduct(int pid, String link) {
        String sql = "INSERT INTO [dbo].[ProductIMG]\n"
                + "           ([ProID]\n"
                + "           ,[Path])\n"
                + "     VALUES\n"
                + "           (?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ps.setString(2, link);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        ProductDAO dao = new ProductDAO();
        List<String> li = new ArrayList<>();
        li.add("2");
        li.add("3");
        for (String string : li) {
            dao.addImageOfProduct(6, string);
        }
    }
}
