/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.AccountDTO;
import DTO.Cart;
import DTO.ItemDTO;
import DTO.OrderDTO;
import DTO.OrderDetail;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author msi
 */
public class OrderDAO extends DBcontext {

    public void addOrder(AccountDTO a, Cart cart, String address, String voucherCode) {
        LocalDate curDate = LocalDate.now();
        String date = curDate.toString();
        try {
            //add order
            String sql = "INSERT INTO ORDERS VALUES (?,?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, a.getAccountID());
            st.setDouble(2, cart.getTotalMoney());
            st.setString(3, date);
            st.setString(4, address);
            st.setInt(5, 0);
            st.setString(6, voucherCode);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public int getLastOrderID() {
        String sql = "select top 1 OrderID from Orders order by OrderID desc";
        int id = 0;
        try {
            PreparedStatement st1 = connection.prepareStatement(sql);
            ResultSet rs = st1.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return id;
    }

    public void addOrderDetail(Cart cart, int oid) {
        String sql = "INSERT INTO OrderDetail VALUES (?,?,?,?,?)";
        try {
            for (ItemDTO i : cart.getItems()) {
                String sql_2 = "INSERT INTO OrderDetail VALUES (?,?,?,?)";
                PreparedStatement st2 = connection.prepareStatement(sql_2);
                st2.setInt(1, i.getProduct().getProductID());
                st2.setInt(2, oid);
                st2.setInt(3, i.getQuantity());
                st2.setDouble(4, i.getProduct().getPrice() /*tru di tien discount*/);
                st2.executeUpdate();
            }
        } catch (Exception e) {
        }

    }

    public void updateQuantity(Cart cart) {
        String sql_3 = "UPDATE Product SET QuantityAvailable = QuantityAvailable - ? WHERE ProductID = ?";
        try {
            PreparedStatement st3 = connection.prepareStatement(sql_3);
            for (ItemDTO i : cart.getItems()) {
                st3.setInt(1, i.getQuantity());
                st3.setInt(2, i.getProduct().getProductID());
                st3.executeUpdate();
            }
        } catch (Exception e) {
        }
    }

    public List<OrderDTO> listOrderInAdminHome(int number) {
        String sql = "SELECT TOP " + number + " od.OrderID,od.OrderDate, a.Fullname, od.TotalPrice, od.Status FROM Orders od\n"
                + "JOIN Account a \n"
                + "ON a.AccountID = od.AccountID ORDER BY od.OrderID DESC";
        List<OrderDTO> lo = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderDTO ord = new OrderDTO();
                ord.setOrderID(rs.getInt(1));
                ord.setOrderDate(rs.getString(2));
                ord.setFullname(rs.getString(3));
                ord.setTotalPrice(rs.getDouble(4));
                ord.setStatus(rs.getInt(5));
                lo.add(ord);
            }
        } catch (Exception e) {
        }
        return lo;
    }

    public double todaySale() {
        // Lấy ngày hiện tại
        java.util.Date date = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        double value = 0;
        // Tạo câu truy vấn SQL
        String query = "SELECT SUM(TotalPrice) AS TotalPriceSum FROM Orders WHERE OrderDate = '" + sqlDate + "' AND Status = 1";
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                value = rs.getDouble(1);
            }
        } catch (Exception e) {
        }
        return value;
    }

    public List<OrderDTO> myPurchase(int id) {
        String sql = "SELECT o.OrderID, a.Fullname, o.TotalPrice, o.OrderDate,o.Address,o.Status FROM Orders o JOIN Account a ON a.AccountID = o.AccountID WHERE a.AccountID = ?";
        List<OrderDTO> lo = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderDTO o = new OrderDTO();
                o.setOrderID(rs.getInt(1));
                o.setFullname(rs.getString(2));
                o.setTotalPrice(rs.getDouble(3));
                o.setOrderDate(rs.getString(4));
                o.setAddress(rs.getString(5));
                o.setStatus(rs.getInt(6));
                lo.add(o);
            }
        } catch (Exception e) {
        }
        return lo;
    }

    public List<OrderDetail> orderDetail(int oid) {
        String sql = "SELECT od.OrderDetailID, od.OrderID,p.Name,od.Quantity,od.UnitPrice FROM OrderDetail od\n"
                + "JOIN Product p \n"
                + "ON p.ProductID = od.ProductID WHERE od.OrderID = ?";
        List<OrderDetail> lod = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, oid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderDetail o = new OrderDetail();
                o.setOrderDetailID(rs.getInt(1));
                o.setOrder_id(rs.getInt(2));
                o.setPname(rs.getString(3));
                o.setQuantity(rs.getInt(4));
                o.setOprice(rs.getDouble(5));
                lod.add(o);
            }
        } catch (Exception e) {
        }
        return lod;
    }

    public void changeStatusOrder(int id, int status) {
        String sql = "UPDATE [dbo].[Orders]\n"
                + "   SET [Status] = ?\n"
                + " WHERE OrderID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        OrderDAO dao = new OrderDAO();
        dao.changeStatusOrder(28, 0);
    }

}
