/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Product;

import DAO.CategoryDAO;
import DAO.ProductDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author msi
 */
@WebServlet(name = "TableProductServlet", urlPatterns = {"/tableProduct"})
public class TableProductServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO productDAO = new ProductDAO();
        CategoryDAO cateDAO = new CategoryDAO();
        request.setAttribute("listProduct", productDAO.getAllProduct());
        request.setAttribute("cate", cateDAO.getAllCategory());
        request.getRequestDispatcher("TableProduct.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pname = request.getParameter("name");
        int cate = Integer.parseInt(request.getParameter("cate"));
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        double price = Double.parseDouble(request.getParameter("price"));
        String ram = request.getParameter("ram");
        String storage = request.getParameter("storage");
        String cpu = request.getParameter("cpu");
        String vga = request.getParameter("vga");
        ProductDAO pdao = new ProductDAO();
        response.setContentType("text/html;charset=UTF-8");
        Enumeration<String> parameterNames = request.getParameterNames();
        List<String> lcm = new ArrayList<>();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            if (paramName.contains("Image")) {
                lcm.add(request.getParameter(paramName));
            }
        }
        pdao.addProduct(cate, pname, quantity, price, ram, storage, cpu, vga, 0);
        int pid = pdao.getPidJustAdd();
        for (String i : lcm) {
            pdao.addImageOfProduct(pid, i);
        }

        response.sendRedirect("tableProduct");

    }

}
